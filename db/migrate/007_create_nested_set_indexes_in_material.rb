class CreateNestedSetIndexesInMaterial < ActiveRecord::Migration
  def change
    add_index :materials, :lft
    add_index :materials, :rgt
  end
end