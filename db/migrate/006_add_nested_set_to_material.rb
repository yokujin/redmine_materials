class AddNestedSetToMaterial < ActiveRecord::Migration
  def self.up
    add_column :materials, :lft,       :integer, :index => true
    add_column :materials, :rgt,       :integer, :index => true

    # optional fields
    add_column :materials, :depth,          :integer, :null => false, :default => 0
    add_column :materials, :children_count, :integer, :null => false, :default => 0

    # update :lft and :rgt columns
    Material.rebuild!
  end

  def self.down
    remove_column :materials, :lft
    remove_column :materials, :rgt

    # optional fields
    remove_column :materials, :depth
    remove_column :materials, :children_count
  end
end